<?php

if (!function_exists('api_response_data')) {
    function api_response_data($success = false, $data = null, $flash_messages = null, $code = null)
    {
        $codeStatus = $code;
        
        if (!$code) {
            if ($success) {
                $codeStatus = 200;
            } else {
                $codeStatus = 422;
            }
        }
        
        return response()->json([
            'success' => $success,
            'data' => $data,
            'message' => $flash_messages,
        ], $codeStatus);
    }
}

if (!function_exists('flash_messages')) {
    function flash_messages($successMsg = null, $dangerMsg = null, $warningMsg = null)
    {
        $msg = [];
        if ($successMsg) {
            data_set($msg, 'success', $successMsg);
        }
        if ($dangerMsg) {
            data_set($msg, 'danger', $dangerMsg);
        }
        if ($warningMsg) {
            data_set($msg, 'warning', $warningMsg);
        }
        
        return $msg;
    }
}
