<?php

namespace App\Console\Commands;

use App\Jobs\ExportRequestJob;
use Illuminate\Console\Command;

class ExportRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export_request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch all pending export requests job.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \App\Models\ExportRequest::whereStatus('pending')
        ->each(function (\App\Models\ExportRequest $exportRequest) {
            dispatch(new ExportRequestJob($exportRequest));
        });
    }
}
