<?php

namespace App\Providers;

use App\Events\ExportRequestSubmitted;
use App\Events\SurveyImportEvent;
use App\Listeners\AttachSurveysToExportRequestListener;
use App\Listeners\ProcessExportRequestJobListener;
use App\Listeners\ProcessSurveyImportJobListener;
use App\Listeners\SaveExportRequestListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        
        ExportRequestSubmitted::class => [
            AttachSurveysToExportRequestListener::class,
            ProcessExportRequestJobListener::class,
        ],
        
        SurveyImportEvent::class => [
            ProcessSurveyImportJobListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
