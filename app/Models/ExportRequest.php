<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class ExportRequest extends Model
{
    use SearchableTrait;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference', 'from',
        'to', 'status'
    ];
    
    protected $casts = [
        'from' => 'datetime',
        'to' => 'datetime',
    ];
    
    const PENDING_STATUS = 'pending';
    const COMPLETE_STATUS = 'complete';
    const FAIL_STATUS = 'fail';
    
    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'export_requests.reference' => 10,
            'export_requests.status' => 5,
        ]
    ];
    
    public function surveys() {
        return $this->belongsToMany(Survey::class);
    }
    
    public function export_data() {
        return $this->hasOne(ExportData::class);
    }
    
}
