<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Survey extends Model
{
    use SearchableTrait;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname',
        'email', 'age', 'telephone'
    ];
    
    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'surveys.firstname' => 10,
            'surveys.lastname' => 10,
            'surveys.email' => 10,
            'surveys.telephone' => 5,
            'surveys.age' => 2
        ]
    ];
    
    public function exportRequests() {
        return $this->belongsToMany(ExportRequest::class);
    }
    
    public function scopeWhereDateBetween($query, $date_from, $date_to)
    {
        return $query->whereDate('created_at', '>=', $date_from)
            ->whereDate('created_at', '<=', $date_to);
    }
}
