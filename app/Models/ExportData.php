<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class ExportData extends Model
{
    protected $table = 'export_datas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'page',
        'export_request_id',
        'file_path',
    ];
    
    public function exportRequest() {
        return $this->belongsTo(ExportRequest::class);
    }
    
    public function getFileUrlAttribute()
    {
        return Storage::url($this->file_path);
    }
}
