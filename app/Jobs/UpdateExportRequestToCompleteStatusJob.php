<?php

namespace App\Jobs;

use App\Models\ExportRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateExportRequestToCompleteStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $exportRequest;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ExportRequest $exportRequest)
    {
        $this->exportRequest = $exportRequest;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->exportRequest->status = ExportRequest::COMPLETE_STATUS;
        $this->exportRequest->save();
    }
}
