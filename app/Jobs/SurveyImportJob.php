<?php

namespace App\Jobs;

use App\Imports\SurveyImport;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;


class SurveyImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $excelFilePath;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($excelFilePath)
    {
        $this->excelFilePath = "app/public/$excelFilePath";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Excel::import(new SurveyImport(), storage_path($this->excelFilePath));
        //Delete excel temp_file after import done in queue.
        Storage::delete($this->excelFilePath);
    }
}
