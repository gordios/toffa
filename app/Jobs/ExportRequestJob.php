<?php

namespace App\Jobs;

use App\Exports\SurveyExport;
use App\Models\ExportData;
use App\Models\ExportRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class ExportRequestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $exportRequest;
    protected $filename;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ExportRequest $exportRequest)
    {
        $this->exportRequest = $exportRequest;
        $this->filename = uniqid().'.csv';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Delete old export data
        $this->deleteOldExportData();
        
        $exportData = $this->createExportData();
        
        (new SurveyExport($this->exportRequest))
            ->queue($exportData->file_path)->chain([
                new UpdateExportRequestToCompleteStatusJob($this->exportRequest)
        ]);
    }
    
    private function createExportData()
    {
        $startDate = $this->exportRequest->from->format('d/m/Y');
        $endDate = $this->exportRequest->to->format('d/m/Y');
        $title = "Survey list from $startDate to $endDate";
        
        $total = $this->exportRequest->surveys()->count();
        $modulo = $total % 100;
        $pageTotal = (($total - $modulo) / 100) + ($modulo > 0);
        
        $path = 'export/'.$this->filename;
        $exportData = new ExportData();
        $exportData->title = $title;
        $exportData->page = "$pageTotal";
        $exportData->export_request_id = $this->exportRequest->id;
        $exportData->file_path = $path;
        $exportData->save();
        
        return $exportData;
        
    }
    
    private function deleteOldExportData() {
        $oldExportData = data_get($this->exportRequest, 'export_data');
        Storage::delete(data_get($oldExportData, 'file_path'));
        $this->exportRequest->export_data()->delete();
    }
}
