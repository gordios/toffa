<?php

namespace App\Http\Requests\Api\Survey;

use App\Http\Requests\Api\ApiFormRequest;

class StoreRequest extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'survey_csv' => ['required', 'bail', 'file', 'mimes:xlsx,xls,csv,txt'],
        ];
    }
}
