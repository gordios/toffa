<?php

namespace App\Http\Controllers\Api;

use App\Events\SurveyImportEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Survey\StoreRequest;
use App\Models\Survey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->input('q');
    
        $surveys = Survey::search($q)->latest('id')->paginate(15);
        $surveys->appends($request->except('page'));
    
        return api_response_data(true, $surveys,
            flash_messages('Survey list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        event( new SurveyImportEvent($request->file('survey_csv')));
    
        return api_response_data(true, null,
            flash_messages('Processing surveys creation...'));
    }
}
