<?php

namespace App\Http\Controllers\Api;

use App\Events\ExportRequestSubmitted;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ExportRequest\StoreRequest;
use App\Models\ExportRequest;
use Illuminate\Http\Request;

class ExportRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->input('q');
    
        $export_requests = ExportRequest::with('export_data')
            ->withCount('surveys')
            ->latest('id')
            ->search($q)
            ->paginate(15);
        $export_requests->appends($request->except('page'));
    
        return api_response_data(true, $export_requests,
            flash_messages('Export requests list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $fromDate = $request->input('export_request.from');
        $toDate = $request->input('export_request.to');
        
        $exportRequest = new ExportRequest();
        $exportRequest->reference = uniqid('EXP');
        $exportRequest->from = $fromDate;
        $exportRequest->to = $toDate;
        $exportRequest->save();
        
        event(new ExportRequestSubmitted($exportRequest));
    
        return api_response_data(true, $exportRequest,
            flash_messages("Export requests created with success. Wait or make another GET request with parameter ?q={$exportRequest->reference} to check the status !"));
    }
    
}
