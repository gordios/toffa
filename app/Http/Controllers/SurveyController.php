<?php

namespace App\Http\Controllers;

use App\Events\SurveyImportEvent;
use App\Http\Requests\Survey\StoreRequest;
use App\Models\Survey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->input('q');
    
        $surveys = Survey::search($q)->latest('id')->paginate(15);
        $surveys->appends($request->except('page'));
    
        return view('survey.index', compact('surveys', 'q'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('survey.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        event( new SurveyImportEvent($request->file('survey_csv')));
        $request->session()->flash('success', 'Processing surveys creation...');
        
        return redirect()->route('survey.index');
    }
}
