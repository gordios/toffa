<?php

namespace App\Http\Controllers;

use App\Events\ExportRequestSubmitted;
use App\Http\Requests\ExportRequest\StoreRequest;
use App\Models\ExportRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ExportRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->input('q');
    
        $export_requests = ExportRequest::with('export_data')
            ->withCount('surveys')
            ->latest('id')
            ->search($q)
            ->paginate(15);
        $export_requests->appends($request->except('page'));
    
        return view('export_request.index', compact('export_requests', 'q'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('export_request.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $fromDate = $request->input('export_request.from');
        $toDate = $request->input('export_request.to');
        
        $exportRequest = new ExportRequest();
        $exportRequest->reference = uniqid('EXP');
        $exportRequest->from = $fromDate;
        $exportRequest->to = $toDate;
        $exportRequest->save();
        
        event(new ExportRequestSubmitted($exportRequest));
    
        $request->session()->flash('success', "Processing export request.
        Wait few minutes and reload the page or check the request status
        with reference <strong>{$exportRequest->reference}</strong>.");
        
        return redirect()->route('export_request.index');
    }
    
}
