<?php

namespace App\Exports;

use App\Models\ExportRequest;
use App\Models\Survey;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SurveyExport implements FromQuery, ShouldAutoSize, WithHeadings
{
    use Exportable;
    
    protected $exportRequest;
    
    public function __construct(ExportRequest $exportRequest)
    {
        $this->exportRequest = $exportRequest;
    }
    
    public function query()
    {
        return $this->exportRequest->surveys();
        
        /*return Survey::whereDateBetween(
            $this->exportRequest->from,
            $this->exportRequest->to
        );*/
    }
    
    public function headings(): array
    {
        return [
            '#',
            'Firstname',
            'Lastname',
            'Email',
            'Telephone',
            'Age',
        ];
    }
}
