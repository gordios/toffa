<?php

namespace App\Listeners;

use App\Models\Survey;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AttachSurveysToExportRequestListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $exportRequest = $event->exportRequest;
    
        $fromDate = $event->exportRequest->from->format('Y-m-d');
        $toDate = $event->exportRequest->to->format('Y-m-d');
    
        $survey_ids = Survey::whereDateBetween(
            $fromDate, $toDate
        )->pluck('id')->toArray();
        
        collect($survey_ids)->chunk(1000)->each(
            function ($ids) use ($exportRequest)
            {
                $exportRequest->surveys()->attach($ids);
            }
        );
    }
}
