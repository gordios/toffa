<?php

namespace App\Listeners;

use App\Jobs\ExportRequestJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProcessExportRequestJobListener implements ShouldQueue
{
    public $timeout = 0;
    
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        dispatch(new ExportRequestJob($event->exportRequest));
    }
}
