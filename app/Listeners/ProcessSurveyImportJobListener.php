<?php

namespace App\Listeners;

use App\Jobs\SurveyImportJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\UploadedFile;

class ProcessSurveyImportJobListener
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $excelFilePath = $this->storeFileInTempFolder($event->excelCsvFile);
        dispatch(new SurveyImportJob($excelFilePath));
    }
    
    private function storeFileInTempFolder(UploadedFile $excelCsvFile)
    {
        $filename = uniqid().'.csv';
        $folder = "imports";
        return $excelCsvFile->storeAs($folder, $filename);
    }
}
