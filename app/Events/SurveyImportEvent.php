<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Http\UploadedFile;

class SurveyImportEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $excelCsvFile;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(UploadedFile $excelCsvFile)
    {
        $this->excelCsvFile = $excelCsvFile;
    }
    
}
