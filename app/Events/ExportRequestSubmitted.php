<?php

namespace App\Events;

use App\Models\ExportRequest;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ExportRequestSubmitted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $exportRequest;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ExportRequest $exportRequest)
    {
        $this->exportRequest = $exportRequest;
    }
    
}
