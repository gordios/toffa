<?php

namespace App\Imports;

use App\Models\Survey;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class SurveyImport implements ToModel, WithHeadingRow,
                              WithBatchInserts, WithChunkReading,
                              WithValidation, SkipsOnFailure, SkipsOnError
                              
{
    use Importable, SkipsFailures, SkipsErrors;
    
    /**
     * @param array $row
     * @return Survey
     */
    public function model(array $row)
    {
        $survey =  new Survey([
            'firstname' => $row['firstname'],
            'lastname' => $row['lastname'],
            'email' => $row['email'],
            'telephone' => $row['telephone'],
            'age' => $row['age'],
        ]);
        
        return $survey;
    }
    
    public function rules(): array
    {
        return [
            '*.firstname' => ['required'],
            '*.lastname' => ['required'],
            '*.email' => ['required', 'email'],
            '*.telephone' => ['required'],
            '*.age' => ['required', 'numeric'],
        ];
    }
    
    public function chunkSize(): int
    {
        return config('excel.exports.chunk_import_size');
    }
    
    public function batchSize(): int
    {
        return config('excel.exports.chunk_import_size');
    }
    
    /**
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        // Handle the failures how you'd like.
    
       /* foreach ($failures as $failure) {
            $failure->row(); // row that went wrong
            $failure->attribute(); // either heading key (if using heading row concern) or column index
            $failure->errors(); // Actual error messages from Laravel validator
            $failure->values(); // The values of the row that has failed.
        }*/
    }
}
