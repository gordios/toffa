<table>
    <thead class="text-center">
        <tr>
            <th class="align-middle">#</th>
            <th class="align-middle">firstname</th>
            <th class="align-middle">lastname</th>
            <th class="align-middle">email</th>
            <th class="align-middle">telephone</th>
            <th class="align-middle">age</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($surveys->chunk(100) as $collection)
            @foreach ($collection as $survey)
                <tr class="text-center">
                    <td>{{$survey->id}}</td>
                    <td>{{$survey->firstname}}</td>
                    <td>{{$survey->lastname}}</td>
                    <td>{{$survey->email}}</td>
                    <td>{{$survey->telephone}}</td>
                    <td>{{$survey->age}}</td>
                </tr>
            @endforeach
        @endforeach
    </tbody>
</table>
