@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-12">

                <p class="text-right">
                    {!! link_to_route('export_request.create', 'Create export request', null, ['class' => 'btn btn-md btn-info']) !!}
                </p>

                <div class="card">
                    <div class="card-header bg-info">
                        <div class="row justify-content-between">
                            <div class="col-md-3">
                                <h4>Export requests list</h4>
                            </div>

                            <div class="col-md-4">
                                <form class="form-inline pull-right">
                                    <input class="form-control mr-sm-2" type="search" value="{{$q}}" name="q" placeholder="Search reference." aria-label="Search">
                                    <button class="btn btn-outline-white my-sm-0 my-3" type="submit">Search</button>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Reference</th>
                                        <th scope="col">From</th>
                                        <th scope="col">To</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Surveys</th>
                                        <th class="text-center">Files</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($export_requests as $export_request)
                                    <tr>
                                        <td>{{$export_request->id}}</td>
                                        <td>{{$export_request->reference}}</td>
                                        <td>{{$export_request->from->format('d/m/Y')}}</td>
                                        <td>{{$export_request->to->format('d/m/Y')}}</td>
                                        <td>{{$export_request->status}}</td>
                                        <td>{{$export_request->surveys_count}}</td>
                                        <td>
                                            @if($export_request->status == 'pending')
                                                Waiting...
                                            @else
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a class="btn btn-sm btn-info mb-2" href="{{$export_request->export_data->file_url}}" download>
                                                            Download excel file
                                                        </a>
                                                    </div>
                                                </div>

                                            @endif
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="5">No export request data.</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <div class="card-footer">
                        {{$export_requests->links()}}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
