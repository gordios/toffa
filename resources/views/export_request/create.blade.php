@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-8">

                <p class="text-right">
                    {!! link_to(URL::previous(), 'Go back', ['class' => 'btn btn-md btn-info']) !!}
                </p>

                <div class="card">
                    <div class="card-header bg-info">
                        <div class="row justify-content-start">
                            <div class="col-md-6">
                                <h4>Create export request</h4>
                            </div>
                        </div>

                    </div>

                    <div class="card-body">
                        {!! Form::open(['route' => 'export_request.store', 'method' => 'post']) !!}
                            <div class="form-group">
                                {!! Form::label('export_request[from]', 'From') !!}
                                {!! Form::date('export_request[from]', now()->format('Y-m-d'), ['class' => 'form-control', 'required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('export_request[to]', 'To') !!}
                                {!! Form::date('export_request[to]', now()->format('Y-m-d'), ['class' => 'form-control', 'required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Save request', ['class' => 'btn btn-sm btn-info']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
