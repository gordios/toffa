@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-12">

                <p class="text-right">
                    {!! link_to_route('survey.create', 'Add survey', null, ['class' => 'btn btn-md btn-info']) !!}
                </p>

                <div class="card">
                    <div class="card-header bg-info">
                        <div class="row justify-content-between">
                            <div class="col-md-3">
                                <h4>Survey data list</h4>
                            </div>

                            <div class="col-md-4">
                                <form class="form-inline pull-right">
                                    <input class="form-control mr-sm-2" type="search" value="{{$q}}" name="q" placeholder="Search any data." aria-label="Search">
                                    <button class="btn btn-outline-white my-sm-0 my-3" type="submit">Search</button>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Firstname</th>
                                        <th scope="col">Lastname</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Telephone</th>
                                        <th scope="col">Age</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($surveys as $survey)
                                    <tr>
                                        <td>{{$survey->id}}</td>
                                        <td>{{$survey->firstname}}</td>
                                        <td>{{$survey->lastname}}</td>
                                        <td>{{$survey->email}}</td>
                                        <td>{{$survey->telephone}}</td>
                                        <td>{{$survey->age}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="5">No survey data.</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <div class="card-footer">
                        {{$surveys->links()}}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
