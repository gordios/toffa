@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-8">

                <p class="text-right">
                    {!! link_to(URL::previous(), 'Go back', ['class' => 'btn btn-md btn-info']) !!}
                </p>

                <div class="card">
                    <div class="card-header bg-info">
                        <div class="row justify-content-between">
                            <div class="col-md-3">
                                <h4>Create survey</h4>
                            </div>
                        </div>

                    </div>

                    <div class="card-body">
                        {!! Form::open(['route' => 'survey.store', 'method' => 'post', 'files' => true]) !!}
                            <div class="form-group">
                                {!! Form::label('survey_csv', 'Select your CSV file') !!}
                                {!! Form::file('survey_csv', ['class' => 'form-control-file', 'accept' => '.csv']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Save', ['class' => 'btn btn-sm btn-info']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
