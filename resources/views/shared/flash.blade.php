@if (session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show my-3" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {!!  session()->get('success') !!}
    </div>
@endif

@if (session()->has('status'))
    <div class="alert alert-success alert-dismissible fade show my-3" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {!!  session()->get('status') !!}
    </div>
@endif

@if (session()->has('warning'))
    <div class="alert alert-warning alert-dismissible fade show my-3" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {!! session()->get('warning') !!}
    </div>
@endif

@if (session()->has('error'))
    <div class="alert alert-danger alert-dismissible fade show my-3" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {!! session()->get('error') !!}
    </div>
@endif
