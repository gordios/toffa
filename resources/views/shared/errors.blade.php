@if(count( $errors ) > 0)
    <div class="alert alert-danger alert-dismissible fade show my-3">
        <ul class="mb-0 py-2">
            @foreach($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
