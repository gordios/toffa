<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p>This repo is functionality complete — welcome !</p>

----------

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.8/installation#installation)

Clone the repository

    git clone git@gitlab.com:gordios/toffa.git

Switch to the repo folder

    cd toffa

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate --seed

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

#
## Describe possible performance optimizations
<p>I think we can create a command running each night to delete unused exportation files created greater than 3 months to save disk space on the server.</p>

## Which things could be done better, than you’ve done it ?
<p>I think we can add email field on survey creation form allowing to send all download files link to the request user email once the worker have finished to create the CSV files.</p>
