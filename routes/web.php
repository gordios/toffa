<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('survey.index');
});

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);

Route::group(['middleware' => 'auth'], function() {
    Route::resource('survey', 'SurveyController')
        ->only(['index', 'create', 'store']);
    
    Route::resource('export_request', 'ExportRequestController')
        ->only('index', 'create', 'store');
});
