<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Survey;
use Faker\Generator as Faker;

$factory->define(Survey::class, function (Faker $faker) {
    $date = $faker->dateTimeBetween('-1 years');
    
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->email,
        'age' => $faker->numberBetween(18, 35),
        'telephone' => $faker->phoneNumber,
        'created_at' => $date,
        'updated_at' => $date,
    ];
});
