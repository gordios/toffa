<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('export_datas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title');
            $table->string('page');
            $table->unsignedBigInteger('export_request_id');
            $table->foreign('export_request_id')->references('id')->on('export_requests');
            $table->string('file_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('export_datas');
    }
}
