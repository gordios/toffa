#!/bin/bash
set -eo pipefail;

# Create storage paths if missing in persistent storage.
mkdir -p storage/app/public
mkdir -p storage/framework/{cache,sessions,testing,views}
mkdir -p storage/logs

#Change storage and public folder permissions
chmod -R 777 storage
chmod -R 777 public

# Run for a confirmation message that it was created successfully.
php artisan storage:link

#clear cache
#php artisan cache:clear

#migrate force database
php artisan migrate --force
