echo "Starting schedule tasks"

while [ true ]
do
    php /app/artisan schedule:run >> /dev/null 2>&1 --verbose --no-interaction
    sleep 60
done